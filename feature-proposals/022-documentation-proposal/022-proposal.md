# Summary

The system will offer various forms throughout the interface for documenting details about resources. With the implementation of a new metadata model (!20), forms may include new properties to be associated with resources upon import, creation, or editing of existing record. Documentation forms are provided in the following interfaces for the purposes specified below:

   * Import - To capture information about an object's form and structure (e.g., media format, order, label)
   * Detail Pages - To allow edits or additions to a resource's record following import and/or use in an emulation session
   * Configuration Page
      * Environment change log - To document changes made to an environment
      * Software cataloging form - To allow edits or additions to software metadata (e.g., descriptive info available once software is installed or attached to drive)

# Motivation
* To generate necessary data for discovery, administration, and automation, the system needs mechanisms to capture this information. We need effective means and capture information but also to control input of certain values (controlled vocabs) to ensure the data we do receive is accurate and therefore useful.

# Analysis

**Business Rules**
- Keyword tags are unique to each node. If a network resource is given a keyword tag it is not shared to other nodes in the network.
- The environment change log must contain one recorded action in order to save the environment.
- Edits to software metadata in the emulation configuration page are not contingent upon saving a derivative environment; users may save edits at any time during their session.
- Users may add custom values if there is not one available in a controlled vocab.
- An object must have at least one object file (e.g., disk image, container file, individual file) associated with it.
- Content objects must have an identifier from a local system that is not EaaSI
- Every resource must have a detail page
- Pre-emulation session workflow steps (e.g., selection of base environment and objects, UI settings config, etc.) must be completed in the Emulation Project interface and not on the detail page
- Users cannot edit metadata of resources in a network

**Use Cases**
*working on it*

**Risks**

| Type | Description |
| ------ | ------ |
| User Behavior | Users assign many tags that only confuse search results or make navigating by keyword facets difficult |
| User Behavior | Users with insufficient knowledge make edits to metadata of local resources |
| Quality Control | Event records do not contain helpful descriptions for determining prior state of environment in revision history |
| System Design | Users may not assume responsibility for cataloging environment and software details |


# Dev Approach

**Design Summary**

**Dependencies**

**Drawbacks**

**Alternatives**
* Considering postponing the majority of additional metadata properties––software version details (beyond title, version #, QID maybe?)––and focusing instead on enhancing support for metadata that is already in the system. Could give us time to further explore use of Wikibase for certain entities and properties that aren't directly related to the system (e.g., software version, product, some hardware details). Would we be able to use WikiDP interface somehow for cataloging instead of building a second data entry form in config interface?

**Work packages and resource estimates**

# Open Questions

# Future Possibilities

# Future Possibilities


