Proposal MR: !20


# Summary
The EaaSI metadata model defines various properties, associated with entities in the EaaSI system, that support discovery, administration, and automation. Resource metadata includes for instance the software installed in an environment, the system requirements of a software version, or event data describing actions completed on a resource. This information is displayed throughout the system in detail pages, resource cards, and the environment config screen. Metadata may also be input into the system through forms in the import workflow, edits to detail pages, and forms in the environment configuration interface.

# Motivation
* Descriptive metadata improves the overall user experience by facilitating discovery of resources and objects
* Metadata captured by EaaSI users will contribute to the continued growth of linked open data describing software, file formats, and other entities related to computing
* Administrative metadata ensures the efficient and trustworthy operation of a node and networks
* Metadata is the basis for automating essential workflows in EaaSI. When we have quality structured metadata we can expand and/or improve the capabilities of our automation services

# Analysis

**Business Rules**
- A resource shared to a network must include all related metadata properties when harvested by another node, except local identifying information
- Users may only see resources they have access to (e.g., “my resources,” local resources, and network resources) in search results
- Users may only see the metadata of resources they have access to (e.g., “my resources,” local resources, and network resources)
- Every resource must have a detail page
- Pre-emulation session workflow steps (e.g., selection of base environment and objects, UI settings config, etc.) must be completed in the Emulation Project interface and not on the detail page
- Resources must first be shared to the node before they may be shared to a network
- Access to resources in a network cannot be immediately removed (e.g., removal requires input from EaaSI tech support)
- Resource metadata cannot be edited once shared unless a copy or derivative environment is created (and then shared back to a network)
- Content objects and environments cannot be shared to a network

**Use Cases**
* Edit content object - Detail Page - https://gitlab.com/eaasi/eaasi/-/blob/sethand-metadata-proposal/feature-proposals/020_metadata-model/use-cases/uc-content-detail-edit.md
* Faceted browse - https://gitlab.com/eaasi/eaasi/-/blob/sethand-metadata-proposal/feature-proposals/020_metadata-model/use-cases/uc-facet-browse.md
* Fork environment - Detail Pagehttps://gitlab.com/eaasi/eaasi/-/blob/sethand-metadata-proposal/feature-proposals/020_metadata-model/use-cases/uc-facet-browse.md
* Keyword search - https://gitlab.com/eaasi/eaasi/-/blob/sethand-metadata-proposal/feature-proposals/020_metadata-model/use-cases/uc-keyword-search.md
* Quick view - https://gitlab.com/eaasi/eaasi/-/blob/sethand-metadata-proposal/feature-proposals/020_metadata-model/use-cases/uc-quick-view.md
* Save new environment - https://gitlab.com/eaasi/eaasi/-/blob/sethand-metadata-proposal/feature-proposals/020_metadata-model/use-cases/user-saves-environment.md

**Risks**

| Type | Description |
| ------ | ------ |
| Quality Control | Lack of detailed metadata records returns an incomplete or confusing set of results |
| Performance | Efficiency of search suffers as more resources and metadata are added to the system |
| System Design | Metadata properties aren’t helpful to users |
| User Behavior | Users with insufficient knowledge make edits to metadata of local resources |
| Quality Control | Event records do not contain helpful descriptions for determining prior state of environment in revision history |
| Quality Control | Users share resources that are incomplete (e.g., missing useful metadata), incorrect (e.g., metadata includes mistakes), or simply don't work |
| Quality Control | Nodes share resources they did not have permission to share and they must be removed |
| Quality Control | Nodes build derivative environments using resources that must then be removed |
| System Design | Unable to maintain consistency in software version data as multiple nodes start contributing |

# Dev Approach

**Design Summary**

*Supporting Documentation*
Metadata Dictionary - https://docs.google.com/spreadsheets/d/1nkrKcuXccQvu4jMA3g07Rb18KKTOL2jLYevTD2Ur_LQ/edit?usp=sharing
Domain Model - *Update in progress*

**Dependencies**


**Drawbacks**

* Metadata migration process required to translate existing infrastructure to any changes


**Alternatives**
* Could implement an incremental approach. Focus on structuring data that is already in the system and implementing core infrastructure for metadata mgmt, display, and use. Second increment could add additional descriptive capabilities for software, better hardware description, etc. 
   * Slim version of MD dictionary - https://docs.google.com/spreadsheets/d/1hAeiSjWchtI8gtfaVXkIjNhAF0vAxZQT5BF6PYHHXTQ/edit?usp=sharing

**Work packages and resource estimates**

# Open Questions
**What is the primary identifier for the main entities?**
* Shall we mint our own identifiers or use existing 
* Not guaranteed that we could get QID immediately or could through Wikibase 
* Unique ID is useful for fast resolving through database 



# Future Possibilities
* Use triplestore database to enable more integration with semantic technologies

