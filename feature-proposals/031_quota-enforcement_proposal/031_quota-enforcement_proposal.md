# Problem to solve
Managing operating costs of the hosted service requires mechanisms for constraining the resources each node will use. A node's quota will constrain the total amount of storage and CPU hours available for adding new objects and images to their node or running environments.

# Editions

[X] Hosted [ ] Local

# Analysis

**Requirements**
* The system must enforce quota limits for CPU hours used by each individual node in the hosted service
* The system must enforce quota limits for storage of private resources in each individual node
* The system must keep track of changes within a nodes private storage as it relates to their total allowed storage quota

**Use Cases**
* Node reaches storage limit
* Node reaches CPU hours limit

**Risks**

| Type | Description |
| ------ | ------ |
| User Behavior | Users or nodes repeatedly use up their storage or CPU quota |
| System Design | Users copy environment images from network, increasing storage costs |

# Proposed Iterations
**1. Multi-tenant object archive**
- 



# Dev Approach

**Work package summary**

- Extend user context to incorporate groups openslx/eaas-server#89
  - username | UID | GUID | role(s)

- Replace Machine's Native-Config with Config-Parameters openslx/eaas-server!17
  - Deprecate the old way of providing machine's configuration using native-configs, that relied on client-side to provide valid emulator's command-line arguments. Instead, the new approach uses simple generic key-value parameters on the client-side, from which emulator-specific command-line arguments are constructed later on the server.

- Client implementation for S3-compatible BlobStore openslx/eaas-server!62

- List private images per user openslx/eaas-server#95
  - ensure ACLs for private images
  - sum-up file size (maybe cache)

- Manage multi tenancy resources openslx/eaas-server#38
  - introduce a tenancy role (JWT claim), only for these users quotas will be enforced (avoid conflicts with (public) landing-page instances and admin/super-users)
  - tenants need to have claims cpu (milli cores) and memory (megabytes). if no value is set, allocation will default to 0 and will fail
  - backend will manage tenants dynamically. once we see a new tenant we check the values provided by the user-token. these values will be used max. quota values.
  - remaining quota will be available via REST for admin users

# Open Questions
- How does copying an image from the network impact quota usage?
- Should quotas be applied at the node level, individual user level, or both?
- When do nodes receive a warning? Are these preemptive? Or do they only see the warning when they reach the limit?
- Are warnings only given to admin users? Or do config users see warning as well? 
