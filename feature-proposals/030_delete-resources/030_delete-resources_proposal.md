# Problem To Solve

While we can't easily delete network resources at this time, nodes may need to manage storage of their private/local resources to lower their storage resource requirements or free up some of their hosted storage quota.

# Editions

[x] Hosted
[x] Local

# Analysis

**Requirements**

* *Objects/Environments shared to a network cannot be deleted*
* If an environment has any derivative, it cannot be deleted (this may change in the future, see below)
* Private software/content object data and metadata must be deleted from storage/database upon request
* If a private base environment has no associated derivatives (e.g., a root environment with just an OS, drivers, utilities), associated image file(s) and metadata must be deleted from storage/database upon request
* A deleted content environment will remove the metadata and image file(s) from the point of content environment creation. The corresponding base environment will not be deleted and remains available in the system
* The system must retain a stub record of deleted resources for audit and reporting purposes

**Use Cases**
* User deletes object
* User deletes base environment
* User deletes content environment

**Risks**
| Type | Description | Potential Mitigation |
| ------ | ------ | ------ |
| User Behavior | User erroneously deletes an environment layer [metadata only] | Could retain files for a period of time an allow restoration of resource |
| System Design | Users unclear on what deletion might mean for related resources | Clear messaging and warnings when users request deletion |
| User Behavior | Users erroneously share resources to a network and need to delete it | To start, this will need to be a training and messaging topic. In the future, we may/should implement a mechanism for removing resources from a network |

# Dev Approach

**Design Summary**

**Dependencies**

**Drawbacks**

**Alternatives**

**Work package summary and size estimates**

# Open Questions
* Should deleting the underlying base environment render all the other derivatives unusable? Example: should content environments be broken if underlying base environment is deleted?

# Future Possibilities
* Deletion of child derivatives from any point in an environment history - for cases in which a software title may need to be deleted from node and/or network
* Deletion of single layer in environment derivative tree - If a private base environment has derivatives, its metadata would be deleted upon request but the system retains any associated image files for use in other derivative environments
* Block analysis to precisely identify matching layers or those containing specific data to delete specific software or content files within an environment and derivatives
