Proposal MR: !17
Proposal Issue: #678

**See Previous Discussion in #392 and #504**

# Problem to solve

Nodes must be able to control who has access to their resources whether within a node, a network, or by end-users. Access control is an integral part of EaaSI workflows and should be used to mark progress through various states of completion or quality. For example, a software object may be imported with incomplete metadata. Limiting its accessibility to an individual user allows them the opportunity to update the metadata before sharing it for use by others in the node. Further updates may be completed before sharing to a network. 

# Impacted Editions

Hosted and Local

# Analysis

### Business Rules/Requirements
- Resources are first associated with an individual user upon creation or import and must be shared to the node to be used by other users in the system
- Resources may only be shared to a node by those who created or imported them or by an admin user
- Node access cannot be revoked once shared
- Resources must first be shared to the node before they may be shared to a network
- Users that access an environment via a shared link must be authenticated and have read-only access
- Base environments and software objects shared to or copied from a network are immediately accessible by all node users (role-permitting)
- Access to resources in a network cannot be immediately removed (e.g., removal requires input from EaaSI tech support)
- Resource metadata cannot be edited once shared unless a copy or derivative environment is created (and then shared back to a network)
- Content objects and environments cannot be shared to a network

### Use Cases

* [User to node sharing](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/017_resource-permissions/017_use-cases/017_user-to-node.md)
* [Sharing end user access link](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/017_resource-permissions/017_use-cases/017_end-user-link.md)

### Risks

| Type | Description |
| ------ | ------ |
| User Behavior | Users stockpile resources without sharing to their node and subsequently the network |
| User Behavior | User shares a resource with the incorrect user |
| Quality Control | Users share resources that are incomplete (e.g., missing useful metadata), incorrect (e.g., metadata includes mistakes), or simply don't work |
| Quality Control | Nodes share resources they did not have permission to share and they must be removed |
| Quality Control | Nodes build derivative environments using resources that must then be removed

# Proposed Iterations

1. Simple permissions framework
- Private resources available to node users (admin and config roles)
- Can share base environments and [software objects](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/832) to network
- Can copy base environments from network

2. [End-user access to environment via link](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/830)
- Environment access interface different from environment config interface https://gitlab.com/eaasi/program_docs/eaasi/-/issues/836

3. User-level "ownership" of resources
- Resources are first associated with the individual user that created or imported - https://gitlab.com/eaasi/program_docs/eaasi/-/issues/831
- Resources may be shared to a node by those who created or imported them - https://gitlab.com/eaasi/program_docs/eaasi/-/issues/835

4. [Extended access permissions](https://gitlab.com/eaasi/program_docs/eaasi/-/issues/828)
- Assignment of individual access permission to end-users
- IP restrictions for end-user access
- Time limits on end-user access
- Assignment of public access to environments

## Future Opportunities
- **Review process for publishing to network:** 

This is another point, where some review-process might be required. This situation could be prevented to some degree by defining a user-role or user-group (e.g. admin or publisher) and requiring the publishing user to have that role/group to be able to publish content.
A simple approvement system would be better for this, depending on the judging ability of reviewers to "understand" resources to be published. It could be very basic, but similar to the merge-request workflow here in GitLab:

* User A prepares an resource R to be published
* User A requests a review from any reviewer B (e.g. any user with review-role or in the reviewer group)
* B gets notified about a pending review request
* B inspects E and approves or denies publishing
* When E was approved, the resource R will be marked as published, else kept private

But to be user-friendly, such a system would require a commenting-system of some form for communication.

- **Revoke access to published resources**
must be possible in some form in real-world usage, even if that would break dependent resources. We probably need to think how that should be handled at an organizational-level, e.g. in an emergency case.

# Dev Response

**Previous remarks**

@krechert's remarks (7/16/2020):

- ACLs need to be maintained on private entities only. Anything that is shared to the node is visible by all EaaSI user (see #626)
- ACLs need to be maintained for 
   - metadata that is only in the current UI database
   - each image layer, that is private (maintained by the backend)
- Additional resources that are retrieved from third-party systems (Preservica) need to be maintained (e.g. is the user allowed to share access to the resource with the "target user". If the authorization is managed by the third-party system, the target user requires access right, if authorization is maintained by EaaSI the sharing user requires sharing / delegation rights.

## Design Summary

This feature is already partially implemented. Here are some ideas, how the current version can be enhanced and refined:

* Each resource is assigned an owner and visibility property
  * *Owner* is used for accounting and quota management
  * *Visibility* in current implementation is handled by multiple archives, but it may make sense to unify them to a single archive and filter all resources depending on context
* On publishing, visibility is updated accordingly (e.g. private, organization, public)
* Optional access permissions are handled separately via roles or capabilities

## Work packages and resource estimates

* List private images per user https://gitlab.com/openslx/eaas-server/-/issues/95
  * ensure ACLs for private images
  * sum-up file size (maybe cache)
* Create endpoint to sign S3 URLs https://gitlab.com/openslx/eaas-server/-/issues/96

# UX/UI Design

* https://xd.adobe.com/spec/6634252b-368b-4b0d-4fa5-477e5fc2a9e4-1005/screen/219a1cde-638a-41ce-8057-b77e97ba0f90/

# Answered Questions

- How does sharing from user to node or node to network impact quota useage in a hosted instance?

- **If I am an Admin-level user, do resources that I copy from the Network go into pool of My Resources?** 

Oleg:We will need a resource-owner for accounting and quota enforcement though. So for node-wide resources we might need a generic node-user/group or something like that. Because of that should the published resources always belong to the publishing user, even in case of imported resources?

Klaus: I'm not sure if I understood the problem, but for shared / network-wide / public resources the EaaSI host is the owner. The managing the scale of replicated environments should be separate issue and not subject to individual user quotas.
Technically speaking, we have already 2 distinct archives. Public images and private images (beloging to a group an individual).
This should simplify accounting...
