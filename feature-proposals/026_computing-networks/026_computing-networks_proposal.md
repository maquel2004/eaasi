# Summary
Computing networks in EaaSI are groups of emulated computing environments communicating via network protocols to enable common client/server or P2P configurations of networked computers. Networks are built from existing environments within an EaaSI node. Users preconfigure network parameters (IP addresses, network name) and complete the network setup while running the environments in emulators. Computing networks are presented as a separate resource type (distinct from environments, software objects, etc.) once saved. 

# Motivation
Networking is an integral component of computer history and many applications and systems rely on network infrastructure to operate. EaaSI must provide this as an option, but foolproof configuration as much as possible, so users can easily set up and operate emulated computing networks.

# Analysis

**Business Rules/Requirements**
- Environments must exist within a node before they can be connected in a computing network. Users cannot create a root environment at the same time as they create a network.
- Network settings may be saved as configured, but the environments within the network can still be revised individually.
- Environments must have an assigned type (e.g., personal computer, server, mobile) so users can easily identify environments to use in a network
- Networks are treated as their own type of resource and can be discovered in their own section of the access interface
- Networks have the same sharing and permissions settings as environments: they can be shared in a network, links can be sent to end users, etc.

**Use Cases**
- User compiles computing network resources in emulation project
- User configures and saves a computing network
- Node copies computing network from another node in an EaaSI network

**Risks**

| Type | Description |
| ------ | ------ |
| User Behavior | Users unfamiliar with typical network configuration practices have difficulty making networks work |
| User Behavior | User unknowingly revises environment that is also part of a computing network erroneously impacting the network functionality |
| User Behavior | Running numerous computing networks for access quickly consumes a hosted node's resource quota |

# Dev Approach

**Design Summary**

**Dependencies**

**Drawbacks**

**Alternatives**

**Work packages and resource estimates**

# Open Questions
- Can I create a derivative network like I can create a derivative environment?
- Should users be able to “live” add an environment to a network? For example, if I start up my application server and make some config changes, could I then (within the config interface) swap to another environment to run the client?
- Who has permission to write back to a server? I'd assume that access users would have read-only access but will config users need to write to server for setup?
- Are we exclusively looking to address TCP/IP networking? (I assume it's the starting point) Would we like to investigate and support the creation of other network types (BBS, Netware/IPX, AppleTalk) in the future? If we say "yes" to the latter, does that affect how this feature is initially designed and set up now?

# Future Possibilities

