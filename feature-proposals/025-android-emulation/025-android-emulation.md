# Summary
Android emulation will mark the first attempt to incorporate emulation of a mobile operating system. The Android operating system will be presented as an option for creation of base environments with corresponding machine configuration (according to emulator capabilities), similar to other templates available for Windows, Machintosh, and Linux operating systems. Mobile software apps will also be incorporated into the EaaSI system as an additional form of software object, with corresponding metadata properties unique to mobile software (if necessary). 

Workflows for configuration of and interaction with Android-based computing environments will depend on the design and capabilities of the Android emulators used in EaaSI. See Dev Approach and Questions below.

# Motivation
The recent past, present, and future of software is likely predominately mobile. We need to determine how to approach mobile emulation in EaaSI and determine whether current workflows are suitable or if special mobile workflows will be required to enable configuration of mobile computing environments and user friendly interaction with mobile interfaces within the EaaSI UI.


# Analysis

**Business Rules/Requirements**
- If possible, the workflows for setting up an android emulation should follow the same process as other systems supported in EaaSI
- Users must be able to input touch-screen based inputs (without physically touching anything) through the EaaSI UI for navigation and interaction with mobile computing environments
- Users should be able to adjust the orientation of the emulation window to mimic the capabilities of mobile devices
- Type (e.g., Android app vs Windows app, mobile environment vs PC environment) must be clearly indicated in resource records on the discovery and detail interfaces of the EaaSI system

**Use Cases**
TBD

**Risks**

| Type | Description |
| ------ | ------ |
| System Design | Interacting with the mobile interface in the EaaSI UI may be awkward or limited functionally |
| System Design | Addition of new emulators may complicate the emulation project and environment configuration workflow as different emulators have varying configuration settings/procedures | 

# Dev Approach

**Design Summary**

**Dependencies**

**Drawbacks**

**Alternatives**

**Work packages and resource estimates**

# Open Questions
- How will users access install packages from within the emulator? Can the files be loaded through some mechanism in the emulator? Or do they need to be downloaded from some location (a mirror we manage?)? [Android Apps can generally be sideloaded (installed outside of an "app store"), however we may want to eventually host a "app store" of sorts, possibly with cracked apps in it, if it simplifies the install process--EMC]

# Future Possibilities
- Running EaaSI on a mobile device?

