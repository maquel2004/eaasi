# Use Case

**Title** Publish base environment or software object to network

**Description** Admin selects environment or software object and publishes it to the network, making it available for sync and use in emulation projects.

**Pre-Condition** Resource shared to node from user resources

**Regular Flow**

1. Admin enters keyword search or browses node contents
2. Admin locates environment or software object in discovery interface
3. a) Admin selects resource card OR b) Admin opens detail page and clicks Actions
4. System displays Action menu [Permitted Admin actions]
5. Admin clicks Publish to network
6. System updates network status of resource

   **Post-condition** Resource metadata available for sync with other nodes; [Hosted edition] Environment available for use by other hosted nodes

**Alternate Flow – Config User**
1. User enters keyword search or browses node contents
2. User locates environment or software object in discovery interface
3. a) User selects resource card OR b) User opens detail page and clicks Actions
4. System displays Action menu [Permitted Config User actions]
5. Publish option not available

   **Post-condition** Resource remains private to node
