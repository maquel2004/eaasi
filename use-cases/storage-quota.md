# Use Case

**Title** Hosted node hits storage quota

**Pre-condition** Private storage quota set during deployment; User logged into system

**Potential Flow – Preemptive Warning**
1. Upon logging in, the System displays a warning informing the user that they have limited remaining space in storage
2. User closes warning window
3. System displays alert text in interface header?
4. User deletes resources or publishes resources to network
5. Once suffficient storage is available, System removes warning from header

  **Post-condition** Node storage back within suitable level and users may create/import resources

**Potential Flow – Reactive Warning**
1. User attempts to Run an emulation project or complete import of an object
2. System confirms whether suffifcient storage is available to create an image layer or import object files
3. System alerts user that there is insufficient storage available to complete this workflow
4. User closes warning window
5. User deletes resources or publishes resource to network

   **Post-condition** User able to run emulation project or complete object import

   **Alternate Flow – User ignores warning – Emulation project**

   4. User clicks Run emulation project

   5. System runs environment

   6. System makes Save Environment action unavailable to user

   **Alternate flow – User ignores warning – Import**

   4. System makes Finish import action unavailable to user

