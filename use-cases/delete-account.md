# Use Case

**Title** Admin deletes user account

**Description** Admin user deletes account from EaaSI node. User is no longer able to login and access system or resources. 

**Pre-Condition** Admin user account created for node; Admin logged into node; User account created

**Regular Flow**
1. Admin opens user account info
2. Admin selects Delete Account
3. System confirms authorization to delete user
4. System prompts user to confirm deletion
5. Admin confirms deletion
6. System deletes user information
7. User attempts to login with deleted credential information

**Post-Condition** User directed to access denied page


