# Use Case

**Title** User deletes content environment

**Description** User locates content environment and deletes it. The system removes files and metadata related to the deleted content environment.

**Pre-condition** Content environment in user's resources or shared to node; Admin user or config user who created environment

**Regular flow**

1. User locates environment in Explore Resources or My Resources interface
2. User selects resource card OR opens detail page and clicks Actions
3. System determines which actions are authorized and displays Action Menu
4. User selects Delete
5. System displays warning regarding results of delete action
6. User selects Delete option
7. System deletes files and metadata of object

   **Post-condition** Content environment removed from node; Content object and base environment still available

