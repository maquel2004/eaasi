# Use Case

**Name** Edit content object

**Description** User edits content object metadata in detail page

**Pre-condition** Content object imported in node

**Ordinary Sequence**
1. The *user* locates the content object in My Resources
2. The *user* clicks the Title field in the resource card
3. The *system* opens the content object's detail page
4. The *system* confirms the user's role and hides/fades actions
5. The *user* clicks the edit button
6. The *system* opens edit mode of the detail page
7. The *user* edits the name of the object's content and changes an object file's format from CD-ROM to Floppy
8. The *user* clicks Save
9. The *system* writes changes to resource record in the database and/or resource metadata files

**Post-condition** Resource metadata is updated to reflect changes made by the user

**Alternate Sequence**

2. The *user* selects the resource card of the content object
3. The *system* displays the Action slider menu
4. The *user* clicks View Details
*Resume Ordinary Sequence at Step 3*
