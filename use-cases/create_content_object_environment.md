**SEE DISCUSSION IN #506 FOR MORE DETAILS**

**Use Case - Create content environment with content object**
-

**Description** Users load content objects (e.g., file sets, CD-ROMs, etc.) into an existing base environment with compatible software to create a content environment for end-user access. Resources are compiled in an emulation project, settings adjusted, and run in an emulator.

**Pre-condition** Content object imported into system or available in network; base environment in node or available in network

**Ordinary Sequence**
1. The *user* selects a content object in the Explore Resources or My Resources interface
2. The *system* displays the resource's action menu
3. The *user* adds the object to their emulation project - *Requirement #333*
4. The *system* adds the content object to their project queue
5. The *user* selects a base environment in the Explore Resources or My Resources interface
6. The *system* displays the resource's action menu
7. The *user* adds the environment to their emulation project
8. The *system* adds the environment to their project queue - *Requirement #333*
9. The *user* opens the Emulation Project interface
10. The *system* shows the contents of the user's queue
11. The *user* selects the base environment from their queue
12. The *system* populates the emulation project interface with base environment details - *Requirement #326*
13. The *system* makes the "Run" button active
14. The *user* selects the content object from their queue
15. The *system* populates the emulation project interface with content object details

    ***In Steps 11-15, the content object may be added to the config before the base environment***

16. The *user* clicks "Run"

**Post-condition** Environment configuration session is started with content object attached to system drive

>>>
**Error Sequence**

15. The *user* attempts to add another software object or content object to the emulation project config
16. The *system* alerts the user they may only add one software or content object at a time *Requirement #325*
>>>