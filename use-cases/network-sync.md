# Use Case

**Title** Admin syncs with network node

**Description** Admin completes metadata synchronization with another node in the network, updating the metadata regarding available environment and software object resources.

**Pre-condition** Node endpoint configured; Local edition only

**Regular Flow – Sync Incremental**

1. Admin opens Metadata Sync interface
2. Admin searches for node or browses list of nodes
3. Admin clicks Sync Incremental for target node
4. System requests and receives new metadata records for recently shared environments and software objects

   **Post-condition** Environment resources available to be saved to node and software objects available for use in emulation projects

**Regular Flow – Sync Full**

1. Admin opens Metadata Sync interface
2. Admin searches for node or browses list of nodes
3. Admin clicks Sync Full for target node
4. System requests and receives all metadata records of shared environments and software objects from node
5. System replaces all metadata records from node in local infrastructure

   **Post-condition** Environment resources available to be saved to node and software objects available for use in emulation projects
