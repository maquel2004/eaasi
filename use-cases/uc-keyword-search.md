# Use Case

**Name** User completes keyword search

**Description**
To quickly discover relevant resources in a node, a user may enter keyword search terms and query the descriptive metadata of aresources available to them and/or narrow the available pool of resources using search facets. Search results display is separated by resource type: environment, software object, content object.

**Pre-Condition:** N/A

**Ordinary Sequence:**
1. The *user* enters term(s) into search box 
2. The *system* queries the inventory of local and network resources available to the user

   **Business rule:** Users may only query metadata of resources they have access to (e.g., “my resources,” local resources, and network resources)
3. The system calculates the relevance ranking of results and compiles facet assignments
4. The system displays the search results according to type and relevance
5. The user selects “See All Environment Results”
6. The system displays only environment resources from search results

**Post-Condition:** Relevant resources from keyword search are displayed for review.

**Error Sequence:**

3. The system finds no relevant results
4. The system displays No Results message
