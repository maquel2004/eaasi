# Use Case

**Name** User browses resources with facets

**Description**

To quickly discover relevant resources in a node, a user may enter keyword search terms and query the descriptive metadata of resources available to them and/or narrow the available pool of resources using search facets. Search results display is separated by resource type: environment, software object, content object.

**Pre-Condition:** N/A

**Ordinary Sequence:**
1. The user opens the Explore Resources page (or My Resources) 
2. The system displays all resources in the node that the user is permitted to access

   **Business rule:** Users may only query metadata of resources they have access to (e.g., “my resources,” local resources, and network resources)

3. The system displays the resources that contain the combination of selected facets

**Post-Condition:** Relevant resources from facets selection are displayed for review.

**Error Sequence:**

3. The system finds no relevant results
4. The system displays No Results message
