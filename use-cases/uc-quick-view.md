# Use Case

**Name** User opens resource quick view

**Description** User navigates to Quick View metadata of a resource

**Pre-condition** Keyword search or facet browse complete; User has the configuration user role

**Ordinary Sequence**

1. The *user* selects the resource card of a software object
2. The *system* confirms the user's role of configuration user
3. The *system* displays the action menu with Save to Node, Publish to Network, and Delete greyed out
4. The *user* selects the Details tab
5. The *system* compiles and displays the quick view properties

**Post condition** Metadata properties displayed in the slider view
