# Use Case

**Title** Admin configures node endpoint for synchronization

**Description** Admin enters endpoint data for another node in the network, tests the connection, and saves the endpoint config.

**Pre-condition** Other node available and endpoint information shared with node admin

**Regular flow**

1. Admin opens Metadata Sync interface
2. Admin clicks Add New Endpoint
3. Admin enters Name and endpoint URL
4. Admin clicks Add Endpoint
5. System confirms the connection
6. System stores information for future sync

   **Post-condition** Endpoint available for sync

**Alternate flow – Endpoint connection fails**

5. System unable to connect to endpoint
6. System prompts user to correct endpoint information
