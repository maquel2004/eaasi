# Use Case

**Name:** User forks environment from revision history in detail page

**Description:** A user reviews the previous states of an environment and determines they would like to create a derivative environment from a previous version.

**Pre-Condition:** Environment has more than one derivative layer.

**Ordinary Sequence:**
1. The user opens an environment’s detail page and navigates to the revision history
2. The system displays a list of the environment’s prior versions with records of changes made
3. The user selects to fork from a prior state of the environment
4. The system adds the prior version of the environment to the user’s emulation project as a base environment

   **Business rule:** Pre-configuration steps (e.g., addition of software, UI settings, etc.) must be completed in the Emulation Project interface and not on the detail page

**Post-Condition:** The forked environment is compiled in an emulation project for the user to run and configure as needed
