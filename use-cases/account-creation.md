# Use Case

**Title** Admin creates new user account

**Description** Admin user enters user data, assigns a role, and saves new account. New user receives notification of account creation.

**Pre-Condition** Admin user account created for node; Admin logged into node

**Regular Flow**
1. Admin selects Create New user
2. System confirms authorization to create user
3. Admin enters email and user name (first/last or username TBD)
4. Admin selects user role
5. Admin saves new user information
6. System records user account information and generates temporary password
7. User receives email notification of new account
8. User opens EaaSI node page
9. User enters email and temporary password
10. System authenticates user credentials
11. System prompts user to change password
12. User enters custom password
13. System records new password

**Post-Condition** New user granted access to EaaSI node

   **Alternate Flow – Login failure**
   
   10. System fails to authenticate user
   11. System prompts user to re-enter credentials (resume at step 10 in Regular Flow) or begin "Forgot password" process (see [password reset use case](https://gitlab.com/eaasi/program_docs/eaasi/-/blob/master/feature-proposals/028_user-mgmt/028_use-cases/028_password-reset.md))

