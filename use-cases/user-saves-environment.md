#Use Case

**Name** User saves new environment

**Description**

When permitted users have completed configuration of an environment, they may save their changes as a new environment or revision. The system guides them through the process, requiring creation of a name and description for new environments. Upon completion of the save process, the system creates a new environment resource, stores the associated disk image, records event information, and sends the user to the environment's detail page.

**Pre-condition**

User has completed Emulation Project configuration or run the environment as is and made changes to the environment (e.g., installed software, changed settings, etc.); Config actions recorded in the change log interface

**Ordinary Sequence**
1. The *user* selects Save Environment
2. The *system* confirms that an entry in the change log is complete
3. The *system* displays the Save Environment dialog
4. The *user* selects New Environment
5. The *system* updates the form to display New Environment properties (e.g., Title, config description)
6. The *user* completes the form
7. The *user* selects Save
8. The *system* creates a new resource record, saves the disk image layer, and generates technical metadata documentation for the new environment
9. The *system* opens the new environment's detail page

**Post condition** New environment is in user's My Resources

**Error Sequence 1**

2. The *system* does not find any entries in the change log
3. The *system* reminds user to record changes in the log
4. The *user* returns to config screen to enter metadata OR selects Ignore and *proceeds from Step 3 of Ordinary Sequence*

**Error Sequence 2**

6. The *user* leaves a required field blank
7. The *system* prompts user to complete required fields
   *Resume from Step 7 of Ordinary Sequence*

**Alternate Sequence**

4. The *user* selects Revision
5. The *system* updates the form to display Revision properties (e.g., revision description)
6. The *user* completes the form
7. The *user* selects Save
8. The *system* creates a new resource record, deprecates the previous version's record, saves the disk image layer, and generates technical metadata documentation for the new environment
9. The *system* opens the new environment's detail page
