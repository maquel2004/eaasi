**Use Case - Create Content Environment from Disk Image**
-

**Description** User creates a content environment from a disk image copy of an existing computer or runtime environment by adding the image to an emulation project and compiling the appropriate machine settings.

**Pre-condition** Content image is imported into system

**Ordinary Sequence**
1. The *user* adds the content image to their emulation project
2. The *system* adds the content image to their project queue - *Requirement #333*
3. The *user* opens the Emulation Project interface
4. The *system* shows the contents of the user's queue
5. The *user* clicks "Select Hardware"
6. The *system* displays the "Select Hardware" interface
7. The *user* selects the system type they need - *Requirement #563*
8. The *system* loads the list of related templates
9. The *user* selects the hardware template to use
10. The *system* populates the emulation project interface with hardware information and drive configuration
11. The *user* updates permitted machine and emulator/UI settings - *Requirement #231*
12. The *user* adds the content image to the boot drive slot in the drive configuration - *Requirement #326*
13. The *system* displays the image in the drive slot
14. The *system* activates the "Run" button active
15. The *user* clicks "Run"

**Post-condition** Environment configuration session is started with boot from content image

>>>
**Error Sequence**

5. The *user* selects the content image from their queue
6. The *system* alerts the user they must first select a hardware template OR does not allow selection of the image until the hardware template is selected

**Alternate Sequence 1**

1. The *user* opens the Emulation Project interface
2. The *system* shows the contents of the user's queue
3. The *user* clicks "Select Hardware"
4. The *system* displays the "Select Hardware" interface
5. The *user* selects the system type they need
6. The *system* loads the list of related templates
7. The *user* selects the hardware template to use
8. The *system* populates the emulation project interface with hardware information and drive configuration
9. The *user* selects "Find Resource"
10. The *system* asks whether the user will look in all node resources or My Resources
11. The *user* selects My Resources
12. The *system* opens the user's My Resources page
13. The *user* selects the content image's resource card
14. The *system* displays the actions menu
15. The *user* clicks Add to Emulation Project
*Resume at Step 11 of Ordinary Sequence*

   **Alternate Sequence 1.1**

   11. The *user* selects Explore Resources
   12. The *system* opens the Explore Resources interface and moves the user's cursor to the search box
   13. The *user* completes a keyword search (see #368)
   *Resume at Step 13 of Alternate Sequence 1*
>>>
