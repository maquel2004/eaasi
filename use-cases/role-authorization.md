# Use Case

**Title** Config user attempts to publish to network

**Description** Config user selects environment in search results. System confirms user's role and displays actions available to user.

**Pre-Condition** User account created with config user role; User logged in; Keyword search completed

**Regular Flow**
1. User selects resource from list of search results
2. System confirms authorization of actions available to user role
3. System displays action menu with publish to network option grayed out and unusable

**Post-Condition** User unable to publish to network
