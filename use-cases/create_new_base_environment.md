**Use Case - Create New Base Environment**
-

**Description** Operating system software can be imported into the EaaSI system as an individual software object. These are used to generate emulated computing environments from scratch in an emulation project. The system asks the user to select an appropriate machine template, configure machine and runtime options, and select the software object to use in install.

**Pre-condition** OS installer software object available in node

**Ordinary Sequence**
1. The *user* adds the software object to their emulation project
2. The *system* adds the software object to their project queue
3. The *user* opens the Emulation Project interface
4. The *system* shows the contents of the user's queue
5. The *user* clicks "Select Hardware"
6. The *system* displays the "Select Hardware" interface
7. The *user* selects the system type they need
8. The *system* loads the list of related templates
9. The *user* selects the hardware template to use
10. The *system* populates the emulation project interface with hardware information and drive configuration
11. The *user* adds the software object to the appropriate drive slot
12. The *system* displays the image in the drive slot
13. The *system* activates the "Run" button active
14. The *user* selects the disk drive as the boot drive
15. The *user* clicks "Run"

**Post-condition** Environment configuration session is started with boot from software object

>>>
**Error Sequence**

5. The *user* selects the software object from their queue
6. The *system* alerts the user they must first select a hardware template OR does not allow selection of the object until the hardware template is selected

**Alternate Sequence 1**

1. The *user* opens the Emulation Project interface
2. The *system* shows the contents of the user's queue
3. The *user* clicks "Select Hardware"
4. The *system* displays the "Select Hardware" interface
5. The *user* selects the system type they need
6. The *system* loads the list of related templates
7. The *user* selects the hardware template to use
8. The *system* populates the emulation project interface with hardware information and drive configuration
9. The *user* selects "Find Resource"
10. The *system* asks whether the user will look in all node resources or My Resources
11. The *user* selects My Resources
12. The *system* opens the user's My Resources page
13. The *user* selects the software object's resource card
14. The *system* displays the actions menu
15. The *user* clicks Add to Emulation Project

*Resume at Step 11 of Ordinary Sequence*

**Alternate Sequence 1.1**

11. The *user* selects Explore Resources
12. The *system* opens the Explore Resources interface and moves the user's cursor to the search box
13. The *user* completes a keyword search (see #368)

*Resume at Step 13 of Alternate Sequence 1*
>>>
