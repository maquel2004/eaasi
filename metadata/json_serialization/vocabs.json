{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "EaaSI - Controlled Vocabs",
    "description": "Properties of various entities/vocabularies used in the EaaSI emulation-as-a-service system",
    "type": "object",
    "properties": {
        "bitRate": {
            "description": "Number of bits that are conveyed or processed per unit of time",
            "type": "object",
            "required": [
                "label"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                }
            }
        },
        "colorDepth": {
            "description": "Number of bits used to represent colors within pixels of computing devices",
            "type": "object",
            "required": [
                "label",
                "bitDepth"
            ],
            "properties": {
                "label": {"type": "string"},
                "bitDepth": {"type": "integer"}
            }
        },
        "region": {
            "description": "Names of global regions - used with OS region settings",
            "type": "object",
            "required": [
                "label"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                }
            }
        },
        "cpuArchitecture": {
            "description": "Set of rules and methods that describe the functionality, organization, and implementation of computer systems",
            "type": "object",
            "required": ["label"],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                }
            }
        },
        "developer": {
            "description": "Individual or organization responsible for the development, production, and/or distribution of a software product",
            "type": "object",
            "required": ["label"],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                }
            }
        },
        "displayResolution": {
            "description": "The number of pixels in each dimension of an image or display",
            "type": "object",
            "required": [
                "label",
                "width",
                "height"
            ],
            "properties": {
                "label": {"type": "string"},
                "width": {"type": "integer"},
                "height": {"type": "integer"}
            }
        },
        "eventType": {
            "description": "Category of action taken within the EaaSI system",
            "type": "string",
            "enum": [
                "uninstall",
                "install",
                "configuration",
                "revision",
                "import",
                "creation",
                "share",
                "save",
                "copy",
                "deletion",
                "run",
                "edit",
                "export"
            ]
        },
        "fileSystem": {
            "description": "Concrete format or program for storing files and directories on a data storage device",
            "type": "object",
            "required": ["label"],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                }
            }
        },
        "frequencyUnit": {
            "description": "The frequency at which the clock generator of a processor can generate pulses, which are used to synchronize the operations of its components",
            "type": "object",
            "required": [
                "label",
                "qid"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                }
            }
        },
        "keyboardLayout": {
            "description": "The mechanical, visual, or functional arrangement of the keys, legends, or key-meaning associations of a computer keyboard",
            "type": "object",
            "required": [
                "label"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                }
            }
        },
        "language": {
            "description": "Particular system of communication, usually named for the region or peoples that use it",
            "type": "object",
            "required": [
                "label",
                "qid"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                }
            }
        },
        "machineInterface": {
            "description": "Types of mechanical connections used to communicate between hardware components of a computing environment",
            "type": "object",
            "required": [
                "label"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                }
            }
        },
        "objectType": {
            "description": "Form of disk image's original storage medium (e.g., hard disk, CD-ROM) or digital format (e.g., file, package)",
            "type": "object",
            "required": [
                "label",
                "qid"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                }
            }
        },
        "networkService": {
            "description": "An application running at the network application layer and above, that provides data storage, manipulation, presentation, communication or other capability",
            "type": "object",
            "required": [
                "label"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "format": "uri"
                },
                "defaultPort": {
                    "type": "array",
                    "uniqueItems": true,
                    "items": {
                        "portNumber": {"type": "string"}
                    }
                }
            }
        },
        "pointerType": {
            "description": "Form of input interface device used to input spatial data to a computer",
            "type": "object",
            "required": [
                "label"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "form": "uri"
                }
            }
        },
        "softwareLicense": {
            "description": "Pre-defined license that governs the use and/or redistribution of software",
            "type": "object",
            "required": [
                "label"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "form": "uri"
                }
            }
        },
        "softwareType": {
            "description": "Genre or form of software",
            "type": "object",
            "required": [
                "label",
                "qid"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid" : {"type":"string","form":"uri"}
            }
        },
        "storageDeviceType": {
            "description": "﻿Data storage device containing recording media (either internally or in the form of a slot for removable media) together with electrical or mechanical parts for the operation of the media",
            "type": "object",
            "required": [
                "label",
                "qid"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "form": "uri"
                }
            }
        },
        "timezone": {
            "description": "Region on Earth that has a uniform standard time for legal, commercial, and social purposes",
            "type": "object",
            "required": [
                "label",
                "qid",
                "utcOffset"
            ],
            "properties": {
                "label": {"type": "string"},
                "qid": {
                    "type": "string",
                    "form": "uri"
                },
                "utcOffset": {"description":"Difference in hours and minutes from Coordinated Universal Time (UTC) for a particular place and date.","type": "string"}
            }
        },
        "unitOfInformation": {
            "description": "Capacity of information storage and communication",
            "type": "object",
            "required": [
                "label",
                "abbreviation",
                "qid"
            ],
            "properties": {
                "label": {"type": "string"},
                "abbreviation": {"type": "string"},
                "qid": {
                    "type": "string",
                    "form": "uri"
                }
            }
        }
    }
}