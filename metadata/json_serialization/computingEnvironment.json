{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "EaaSI - Computing Environment",
    "description": "Properties of emulated computing environments created and managed by EaaSI",
    "type": "object",
    "required": [
        "identifier",
        "source",
        "name",
        "configuredMachine",
        "relatedEvent",
        "derivedFrom"
    ],
    "properties": {
        "identifier": {
            "description": "Unique identifier issued by EaaSI",
            "type": "string"
        },
        "qid": {
            "description": "Wikidata QID of computing environment",
            "type": "string",
            "format": "uri"
        },
        "source": {
            "description": "Name of organization that contributed environment to node and/or network",
            "$ref": "system.json#/properties/organization"
        },
        "name": {
            "description": "The name given to a computing environment",
            "type": "string"
        },
        "description": {
            "description": "Short description of the computing environment's purpose or contents",
            "type": "string"
        },
        "helpText": {
            "description": "Text providing instructions or general guidance for use of the environment",
            "type": "string"
        },
        "configuredMachine": {
            "description": "Configured hardware components emulated as part of the computing environment",
            "type": "object",
            "required": [
                "dateTime",
                "ram",
                "configuredProcessors",
                "configuredKeyboards",
                "configuredDrives",
                "emulator"
            ],
            "properties": {
                "dateTime": {
                    "description": "Assigned date and time settings configured in machine",
                    "type": "string",
                    "format": "date"
                },
                "ram": {
                    "description": "Random access memory (RAM) capacity of hardware environment",
                    "$ref": "#/definitions/ram"
                },
                "rom": {
                    "description": "File containing a copy of the system's read-only memory required for booting and operation of the environment",
                    "$ref": "system.json#/properties/file"
                },
                "bios": {
                    "description": "Firmware used by computing system to initialize hardware during booting and to provide services to operating system and programs",
                    "type": "string"
                },
                "internetAccess": {
                    "description": "Indicator of whether internet access is permitted or not when running the environment",
                    "type": "boolean"
                },
                "configuredProcessors": {
                    "type": "array",
                    "uniqueItems": true,
                    "minItems": 1,
                    "items": {
                        "configuredProcessor": {
                            "description": "Central processing units configured in machine",
                            "type": "object",
                            "required": [
                                "processorDevice",
                                "cores"
                            ],
                            "properties": {
                                "processorDevice": {"$ref": "device.json#/properties/processor"},
                                "cores": {
                                    "description": "Number of CPU cores available to processor",
                                    "type": "integer",
                                    "minimum": 1
                                }
                            }
                        }
                    }
                },
                "configuredKeyboards": {
                    "type": "array",
                    "uniqueItems": true,
                    "minItems": 1,
                    "items": {
                        "configuredKeyboard": {
                            "description": "Configured keyboard input used by system",
                            "type": "object",
                            "required": [
                                "keyboardDevice",
                                "machineInterface"
                            ],
                            "properties": {
                                "keyboardDevice": {"$ref": "device.json#/properties/keyboard"},
                                "machineInterface": {"$ref": "vocabs.json#/properties/machineInterface"}
                            }
                        }
                    }
                },
                "configuredPointers": {
                    "type": "array",
                    "uniqueItems": true,
                    "items": {
                        "configuredPointer": {
                            "description": "Configured pointer input used by system",
                            "type": "object",
                            "required": [
                                "pointerDevice",
                                "machineInterface"
                            ],
                            "properties": {
                                "pointerDevice": {"$ref": "device.json#/properties/pointer"},
                                "machineInterface": {"$ref": "vocabs.json#/properties/machineInterface"}
                            }
                        }
                    }
                },
                "configuredAudioDevices": {
                    "type": "array",
                    "uniqueItems": true,
                    "items": {
                        "configuredAudioDevice": {
                            "description": "Configured audio card used by system",
                            "type": "object",
                            "required": [
                                "audioDevice",
                                "machineInterface"
                            ],
                            "properties": {
                                "audioDevice": {"$ref": "device.json#/properties/audioDevice"},
                                "machineInterface": {"$ref": "vocabs.json#/properties/machineInterface"}
                            }
                        }
                    }
                },
                "configuredGpuDevices": {
                    "type": "array",
                    "uniqueItems": true,
                    "items": {
                        "configuredGpuDevice": {
                            "description": "Configured graphics card used by system",
                            "type": "object",
                            "required": [
                                "gpuDevice",
                                "machineInterface"
                            ],
                            "properties": {
                                "gpuDevice": {"$ref": "device.json#/properties/gpuDevice"},
                                "ram": {"$ref": "#/definitions/ram"},
                                "machineInterface": {"$ref": "vocabs.json#/properties/machineInterface"}
                            }
                        }
                    }
                },
                "configuredNetworkDevices": {
                    "type": "array",
                    "uniqueItems": true,
                    "items": {
                        "configuredNetworkDevice": {
                            "description": "Configured network interface controller used by system",
                            "type": "object",
                            "required": [
                                "networkDevice",
                                "networkService",
                                "servicePort",
                                "machineInterface"
                            ],
                            "properties": {
                                "networkDevice": {"$ref": "device.json#/properties/networkDevice"},
                                "macAddress": {
                                    "description": "Media access control address assigned to network interface controller",
                                    "type": "string"
                                },
                                "networkService": {"$ref": "vocabs.json#/properties/networkService"},
                                "servicePort": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "items": {
                                        "portNumber": {"type": "string"}
                                    }
                                },
                                "machineInterface": {"$ref": "vocabs.json#/properties/machineInterface"}
                            }
                        }
                    }
                },
                "configuredRemovableStorage": {
                    "type": "array",
                    "uniqueItems": true,
                    "items": {
                        "configuredRemovableStorageDevice": {
                            "description": "Removable storage device used by system",
                            "type": "object",
                            "required": [
                                "storageDeviceType",
                                "machineInterface",
                                "bootOrder"
                            ],
                            "properties": {
                                "storageDeviceType": {"$ref": "vocabs.json#/properties/storageDeviceType"},
                                "machineInterface": {"$ref": "vocabs.json#/properties/machineInterface"},
                                "unit": {"type": "integer"},
                                "fileSystem": {"$ref": "vocabs.json#/properties/fileSystem"},
                                "bootOrder": {
                                    "type": "integer",
                                    "minimum": 1
                                }
                            }
                        }
                    }
                },
                "configuredDiskDrives": {
                    "type": "array",
                    "uniqueItems": true,
                    "minItems": 1,
                    "items": {
                        "configuredDrive": {
                            "description": "Configured storage device used by system",
                            "type": "object",
                            "required": [
                                "storageDeviceType",
                                "machineInterface"
                            ],
                            "properties": {
                                "storageDeviceType": {"$ref": "vocabs.json#/properties/storageDeviceType"},
                                "imageFile": {"$ref": "system.json#/properties/file"},
                                "volume": {"$ref": "#/definitions/volume"},
                                "machineInterface": {"$ref": "vocabs.json#/properties/machineInterface"},
                                "unit": {"type": "integer"},
                                "partitions": {
                                    "type": "array",
                                    "uniqueItems": true,
                                    "items": {
                                        "partition": {
                                            "description": "Partition within configured disk that contains a software environment",
                                            "type": "object",
                                            "required": [
                                                "bootOrder",
                                                "softwareEnvironment"
                                            ],
                                            "properties": {
                                                "fileSystem": {"$ref": "vocabs.json#/properties/fileSystem"},
                                                "bootOrder": {
                                                    "type": "integer",
                                                    "minimum": 1
                                                },
                                                "softwareEnvironment": {
                                                    "description": "A configuration of an operating system and other programs captured on a disk image for emulation",
                                                    "type": "object",
                                                    "properties": {
                                                        "configuredOS": {
                                                            "description": "Configured operating system used by the environment",
                                                            "type": "object",
                                                            "required": [
                                                                "manifestationOf",
                                                                "language",
                                                                "dateTime"
                                                            ],
                                                            "properties": {
                                                                "manifestationOf": {
                                                                    "description": "OS version that is manifested by the configured software",
                                                                    "type": "string"
                                                                },
                                                                "installedFrom": {
                                                                    "description": "Software object used to install software in environment",
                                                                    "type": "string"
                                                                },
                                                                "dateTime": {
                                                                    "description": "Date and time assigned in operating system",
                                                                    "type": "string",
                                                                    "format": "date"
                                                                },
                                                                "configuredSettings": {
                                                                    "description": "Various settings configured within the operating system",
                                                                    "type": "object",
                                                                    "properties": {
                                                                        "languageSettings": {
                                                                            "type": "array",
                                                                            "uniqueItems": true,
                                                                            "items": [
                                                                                {
                                                                                    "languageSetting": {
                                                                                        "description": "Language configured for use in operating system",
                                                                                        "$ref": "#/definitions/languageSetting"
                                                                                    },
                                                                                    "primary": {
                                                                                        "description": "Identification of primary language setting used in operating system",
                                                                                        "type": "boolean"
                                                                                    }
                                                                                }
                                                                            ]
                                                                        },
                                                                        "colorDepthSetting": {
                                                                            "description": "Color depth setting selected for use in operating system",
                                                                            "$ref": "#/definitions/colorDepthSetting"
                                                                        },
                                                                        "displayResolutionSetting": {
                                                                            "description": "Display resolution setting selected for use in operating system",
                                                                            "$ref": "#/definitions/displayResolutionSetting"
                                                                        },
                                                                        "regionSetting": {
                                                                            "description": "Region setting selected for use in operating system",
                                                                            "$ref": "#/definitions/regionSetting"
                                                                        },
                                                                        "timezoneSetting": {
                                                                            "description": "Timezone setting selected for use in operating system",
                                                                            "$ref": "#/definitions/timezoneSetting"
                                                                        },
                                                                        "keyboardSetting": {
                                                                            "description": "Keyboard setting configured for use in operating system",
                                                                            "$ref": "#/definitions/keyboardSetting"
                                                                        }
                                                                    }
                                                                },
                                                                "userInformation": {
                                                                    "description": "Details of configured user accounts available in the operating system",
                                                                    "type": "array",
                                                                    "uniqueItems": true,
                                                                    "items": [
                                                                        {
                                                                            "userAccount": {
                                                                                "type": "object",
                                                                                "required": ["userName"],
                                                                                "properties": {
                                                                                    "userName": {
                                                                                        "description": "Name of user account configured in software",
                                                                                        "type": "string"
                                                                                    },
                                                                                    "password": {
                                                                                        "description": "Password associated with user account",
                                                                                        "type": "string"
                                                                                    },
                                                                                    "organization": {
                                                                                        "description": "Name of organization associated with user account",
                                                                                        "type": "string"
                                                                                    },
                                                                                    "pinNumber": {
                                                                                        "description": "Pin number associated with user account (i.e., for login)",
                                                                                        "type": "string"
                                                                                    },
                                                                                    "role": {
                                                                                        "description": "Role assigned to user account in operating system (e.g., Admin, Guest, etc.)",
                                                                                        "type": "string"
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    ]
                                                                }
                                                            }
                                                        },
                                                        "configuredSoftwareSet": {
                                                            "type": "array",
                                                            "uniqueItems": true,
                                                            "items": [
                                                                {
                                                                    "configuredSoftware": {
                                                                        "description": "Software programs installed and configured in the software environment, including applications, drivers, utilities, etc.",
                                                                        "type": "object",
                                                                        "required": [
                                                                            "manifestationOf",
                                                                            "language"
                                                                        ],
                                                                        "properties": {
                                                                            "manifestationOf": {
                                                                                "description": "Software version that is manifested by the configured software",
                                                                                "type": "string"
                                                                            },
                                                                            "installedFrom": {
                                                                                "description": "Software object used to install software in environment",
                                                                                "type": "string"
                                                                            },
                                                                            "language": {
                                                                                "description": "Language configured for use in software",
                                                                                "$ref": "vocabs.json#/properties/language"
                                                                            },
                                                                            "executableLocation": {
                                                                                "description": "Path to location of software executable",
                                                                                "type": "string",
                                                                                "format": "uri"
                                                                            },
                                                                            "executableSyntax": {
                                                                                "description": "Command required to execute software programmatically",
                                                                                "type": "string"
                                                                            },
                                                                            "saveLocation": {
                                                                                "description": "Path to location where software saves outputs",
                                                                                "type": "string",
                                                                                "format": "uri"
                                                                            },
                                                                            "defaultFormatOperations": {
                                                                                "description": "Format operations configured as default for software in environment (may differ from default operation out of the box)",
                                                                                "type": "array",
                                                                                "uniqueItems": true,
                                                                                "items": {"$ref": "#/definitions/formatOperation"}
                                                                            },
                                                                            "userInformation": {
                                                                                "description": "Details of configured user accounts available in the operating system",
                                                                                "type": "array",
                                                                                "uniqueItems": true,
                                                                                "items": {
                                                                                    "userAccount": {
                                                                                        "type": "object",
                                                                                        "required": ["userName"],
                                                                                        "properties": {
                                                                                            "userName": {
                                                                                                "description": "Name of user account configured in software",
                                                                                                "type": "string"
                                                                                            },
                                                                                            "password": {
                                                                                                "description": "Password associated with user account",
                                                                                                "type": "string"
                                                                                            },
                                                                                            "organization": {
                                                                                                "description": "Name of organization associated with user account",
                                                                                                "type": "string"
                                                                                            },
                                                                                            "pinNumber": {
                                                                                                "description": "Pin number associated with user account (i.e., for login)",
                                                                                                "type": "string"
                                                                                            },
                                                                                            "role": {
                                                                                                "description": "Role assigned to user account in operating system (e.g., Admin, Guest, etc.)",
                                                                                                "type": "string"
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            ]
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "emulator": {
                    "description": "Emulator software used to generate the computing environment",
                    "$ref": "system.json#/properties/emulator"
                }
            }
        },
        "uiSettings": {
            "description": "Settings that dictate certain permissions and functionality of EaaSI and emulator systems during access",
            "type": "object",
            "properties": {
                "canPrint": {
                    "type": "boolean"
                },
                "relativeMouse": {
                    "type": "boolean"
                },
                "userFileUpload": {
                    "type": "boolean"
                },
                "webRTCAudio": {
                    "type": "boolean"
                },
                "cleanShutdown": {
                    "type": "boolean"
                }
            }
        },
        "relatedEvent": {
            "description": "Event featuring the computing environment (e.g., creation,revision,etc.)",
            "type": "array",
            "items": {"$ref": "system.json#/properties/event"},
            "uniqueItems": true
        },
        "hasParent": {
            "description": "Computing environment from which the described environment is derived",
            "$ref": "#/properties/identifier"
        },
        "hasChild": {
            "description": "Computing environment derived from described environment",
            "type": "array",
            "items": {"$ref": "#/properties/identifier"},
            "uniqueItems": true
        }
    },
    "definitions": {
        "ram": {
            "type": "object",
            "required": [
                "value",
                "unit"
            ],
            "properties": {
                "value": {
                    "type": "integer",
                    "minimum": 0
                },
                "unit": {"$ref": "vocabs.json#/properties/unitOfInformation"}
            }
        },
        "volume": {
            "type": "object",
            "required": [
                "value",
                "unit"
            ],
            "properties": {
                "value": {
                    "type": "integer",
                    "minimum": 0
                },
                "unit": {"$ref": "vocabs.json#/properties/unitOfInformation"}
            }
        },
        "formatOperation": {
            "type": "object",
            "required": [
                "label",
                "fileExtension",
                "operationType",
                "matchesFormat",
                "default"
            ],
            "properties": {
                "label": {
                    "description": "Label assigned to setting within software (may differ from associated file format)",
                    "type": "string"
                },
                "fileExtension": {
                    "description": "File extension used by software to identify associated format",
                    "type": "string"
                },
                "operationType": {
                    "type": "string",
                    "enum": [
                        "open",
                        "save",
                        "export",
                        "import",
                        "print",
                        "edit"
                    ]
                },
                "matchesFormat": {
                    "description": "Closest matching format version that is being defined by the implementation",
                    "$ref": "vocabs.json#/properties/fileFormat"
                }
            }
        }
    }
}