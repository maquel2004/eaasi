**Base Environment Quick View**
-

| Field                  | Example                                                            |
|------------------------|--------------------------------------------------------------------|
| Name                   | Windows 98 SE Base                                                 |
| Description            | Default installation of Windows 98 Second Edition operating system |
| Hardware Template Name | Generic 90s PC                                                     |
| Operating System Name  | Windows 98 SE                                                      |
| Language               | English                                                            |
| Software Name          | CorelDraw                                                          |
| Language               | English                                                            |
| Internet Enabled       | Yes/No                                                             |
| Emulator               | QEMU                                                               |
| Version                | 3.1                                                                |

**Content Environment Quick View**
-

| Field                  | Example                                                            |
|------------------------|--------------------------------------------------------------------|
| Content Title             | Animales fabulosos : las revistas de Abelardo Castillo / |
| Local Identifier [Source] | 123345 [Voyager]                                         |
| Environment Name          | Windows 98 CD-ROM viewer                                 |
| Description               |                                                          |
| Hardware Template Name    |                                                          |
| Operating System Name     | Windows 98 SE                                            |
| Language                  | English                                                  |
| Software Name             | CorelDraw                                                |
| Language                  | English                                                  |
| Internet Enabled          | Yes/No                                                   |
| Emulator                  | QEMU                                                     |
| Version                   | 3.1                                                      |

**Software Object Quick View**
-

| Field                  | Example                                                            |
|------------------------|--------------------------------------------------------------------|
| Local Identifier [Source] | 123345 [Voyager] |
| Media Format(s)           | Floppy Disk      |
| Primary Software          | AutoCAD R9       |
| Version Number            | 9.0              |
| Version Of                | AutoCAD          |
| Developer                 | AutoDesk         |
| Date Published            | 1987-9           |
| License                   | Apache License   |
| Default Language          | English          |
| Requirements Summary      |                  |
