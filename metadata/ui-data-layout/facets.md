**All Resources**
-

| Facet | Example |
| ------ | ------ |
| Resource Type | Environment, Software, Content |
| Network Status | Private, Public, Remote |
| Source Org | [Node Name] |
| Date Created/Added (Range) | 01-01-2019 - 03-31-2019 |
| Keyword | Statistics |

**Environment Facets**
-

| Facet | Example |
| ------ | ------ |
| Configured OS | Windows 98 SE |
| Configured OS Language | Spanish |
| Configured Software | Microsoft Word 95 |
| Configured Software Language | Spanish |
| Date Created (Range) | 01-01-2019 - 03-31-2019 |
| Date Published to Network (Range) | 01-01-2019 - 03-31-2019 |

**Content Environment Facets**

| Facet | Example |
| ------ | ------ |
| Configured Content Object Media Format | CD-ROM, Floppy Disk, File Set |

**Object Facets**
-

| Facet | Example |
| ------ | ------ |
| Media Format | CD-ROM, Floppy Disk, File Set |

**Software Object Facets**

| Facet | Example |
| ------ | ------ |
| Part of (Software Family) | Microsoft Office |
| Type | Office productivity |
| Developer | Microsoft |
| Language | English |
| Date Published (Range) | 01-01-1994 - 12-31-1999 |
