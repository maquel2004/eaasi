**Base Environment Resource Card**
-

| Field | Example |
| ------ | ------ |
| Name | XP system with MS Office 2000 |
| OS | Windows XP |
| OS Language | English |
| Software | MS Word 2000 |
| Software Language | English |
| Internet enabled | Yes/No |
| Date Created | 2020-07-22 |
| Source Org | Yale University Library |
| Network Status | Local |

**Content Environment Resource Card**
-

| Field | Example |
| ------ | ------ |
| Content Title | Animales fabulosos : las revistas de Abelardo Castillo / |
| Local ID [Source] | 7734947 [BIBID] |
| Name | CD-ROM Viewer PC |
| OS | Windows XP |
| OS Language | English |
| Software | MS Word 2000 |
| Software Language | English |
| Internet enabled | Yes/No |
| Date Created | 2020-07-22 |

**Software Object Resource Card**
-

| Field | Example |
| ------ | ------ |
| Local ID [Source] | 1223445 [BIBID] |
| Object Format(s) | CD-ROM |
| Primary Software | Wordstar 4.0 |
| Software Language | Spanish |
| Date Added | 2020-07-22 |
| Source Org | Yale University Library |
| Network Status | Local |

**Content Object Resource Card**
-

| Field | Example |
| ------ | ------ |
| Local ID [Source] | 1223445 [BIBID] |
| Object Format(s) | CD-ROM |
| Content Title | Animales fabulosos : las revistas de Abelardo Castillo / |
| Date Added | 2020-07-22 |


